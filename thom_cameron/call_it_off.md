Call it Off
===========

| Artist | Thom Cameron |
|--------|--------------|
| Tuning | Standard     |
| Capo   | None         |

Chords
------

### Verse

| Name    | Shape  |
|---------|--------|
| Dmaj13  | 220220 |
| Emaj7   | 021100 |
| G13sus2 | 303030 |
| A       | x02220 |
| Asus2   | x02200 |

### Chorus

| Name  | Shape  |
|-------|--------|
| E     | 022100 |
| Gmaj7 | 320002 |
| F7    | 131211 |
| G7    | 353433 |
| A7    | 575655 |

Verse 1
-------

```
Dmaj13          Emaj7
are we really together
Dmaj13        Emaj7
gathered everywhere
Dmaj13              Emaj7
we swim around like weather
Dmaj13              Emaj7
the fabric looks to tear

                G13sus2
but i'm holding on
     A
i'll wait for you
               Dmaj13
i'll still hold on
```

Solo 1
------

...

Verse 2
-------

```
do you sing a different song?
who taught you what's natural?
your right and your wrong
do they feel like the actual
```

Chorus 1
--------

```
                   G13sus2
i hope it won't be long
        A
till we all call

        E    Gmaj7
call it off
        E    Gmaj7
call it off
F7      G7   A7
```

Verse 3
-------

```
what have we to fight for?
everything is what we've earned
it runs down the drain though
while our backs are turned

and leaves us all
together
together in the storm
```

Solo 2
------

...

Chorus 2
--------

```
i hope it won't be long
till we all call

call it off
you can make it stop
call it off
you can make it stop
call it off
we can make it stop
call it off
we can make it stop
(repeat)
```